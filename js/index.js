$(function(){
    $("[data-toggle='tooltip']").tooltip();

    $('[data-toggle="popover"]').popover();
    
    $('.carousel').carousel({
      interval: 2000
    });

    $('#contacto').on('show.bs.modal', function (e){
      console.log('El modal reserva se esta mostrando');

      $('#contactoBtn').removeClass('btn-dark');
      $('#contactoBtn').addClass('btn-outline-dark');
      $('#contactoBtn').prop('disable', true);
    });

    $('#contacto').on('shown.bs.modal', function (e){
      console.log('El modal reserva se mostro');
    });

    $('#contacto').on('hide.bs.modal', function (e){
      console.log('El modal reserva se oculta');
    });

    $('#contacto').on('hidden.bs.modal', function (e){
      console.log('El modal reserva se oculto');

      $('#contactoBtn').removeClass('btn-outline-dark');
      $('#contactoBtn').addClass('btn-dark');
      $('#contactoBtn').prop('disable', false);
    });
  });